/** Fonction qui génère une chaine partiellement révélée
* selon les lettres passées en param
* @author Christopher
* @param motMystere {string} Le mot mystère à masquer partiellement
* @param lettresSaisies{array} lettres saisies par le joueur 
* @return {string} Chaine partiellement révélée
**/
function afficheMot(motMystere, lettresSaisies) {
	//motMystere = chaine de caractère
	//lettresSaisies = tableau de caractères
	var motPartiel = "";
	
	//Pour chaque lettre de mon mot mystere
	//Je la cherche dans le tableau des lettres saisies
	for (i=0; i<motMystere.length; i++) {
		
		var lettreTrouvee = false;

		//On cherche si la lettre correspond à une des lettres saisies
		for (j=0; j<lettresSaisies.length; j++) {
			if (motMystere[i] == lettresSaisies[j]) {
				//La Iième lettre du mot mystere correspond à la Jième lettre saisie
				lettreTrouvee = true;
				break;
			}
		}
		
		//On regarde si on a trouvé la lettre ou non
		if (lettreTrouvee) {
			motPartiel += motMystere[i];
		} else {
			motPartiel += "_";
		}
	}
	
	return motPartiel;
}

/** 
 
**/

/*
Sélectionner un mot aléatoire parmi un tableau de 10 mots. 
L'utilisateur a 6 essais pour trouver le mot, en proposant des lettres une à une. 
Lorsqu'une lettre proposée est correcte, on affiche le mot avec les lettres correspondantes. 
Lorsqu'une lettre proposée est incorrecte, on affiche une erreur et le nombre d'essais restants. 
La partie se termine lorsque le mot proposé a été trouvé ou qu'il n'y a plus d'essai restant.
*/

/** fonction permettant de jouer au pendu
 *   
 * 
 * 
 * 
 * 
 **/

function jeuPendu() {
	var listeMots = ["Wagon",
	"Velo",
	"Camion",
	"Tortue",
	"Paternel",
	"Renard",
	"Zyklon",
	"Planete",
	"Capitaine",
	"Abandon"];
	
	//Selectionne un mot mystere aléatoire
	var motMystere = listeMots[Math.floor(Math.random()*listeMots.length)];
	
	
	var nbEssais = 6;
	
	var lettreProposee;
	var lettresSaisies = []; // tableau des lettres saisies par le joueur
	
	var partieGagnee = false;
	var motMystereCache;
	
	while (nbEssais>0) {
		motMystereCache = afficheMot(motMystere, lettresSaisies);
		//On affiche le mot en version masquée
		lettreProposee = prompt(motMystereCache+ " Saisir une lettre:");
		
		//J'ajoute ma lettre proposée aux lettres saisies
		lettresSaisies.push(lettreProposee);
		
		if (afficheMot(motMystere, lettresSaisies) == motMystereCache) {
			//LA version d'afficheMot ne change pas
			//On n'a pas trouvé de lettre
			nbEssais--;
		} else {
			//On a trouvé une lettre
			//On réactualise le mot mystere caché
			motMystereCache = afficheMot(motMystere, lettresSaisies);
			
			if (motMystereCache == motMystere) {
				//Le mot a été entièrement révélé, on a gagné
				partieGagnee = true;
				break;
			}
		}
	}
	
	//Afficher le resultat de la partie
	if (partieGagnee) {
		alert("Gagné !");
	} else {
		alert("Perdu");
	}
}






